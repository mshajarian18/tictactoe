﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BoardController : MonoBehaviour
{
    [SerializeField] private BoardSquareCellContainer firstBox , secondBox ,thirdBox ;
    [SerializeField] private BoardStateController boardStateController;
    [SerializeField] private GameObject particle;

    private List<CrossPath> allPaths = new List<CrossPath>();
    public static Action<GameState> OnWin;
    private void Awake()
    {
        BuildCells();
    }

    private void OnEnable()
    {
        MainMenuUiController.OnGameStart += OnGameStart;
    }

    private void OnDisable()
    {
        MainMenuUiController.OnGameStart -= OnGameStart;
    }

    private void OnGameStart()
    {
        boardStateController.Setup();
    }
    
    private void BuildCells()
    {
        for (var i = 0; i < firstBox.Cells.Count; i++)
        {
            var newPath = new CrossPath()
            {
                cells = new List<CrossCell>()
            };
            newPath.cells.Add(firstBox.Cells[i]);
            firstBox.Cells[i].myPaths.Add(newPath);
            firstBox.Cells[i].Setup(ClickCell);
            
            newPath.cells.Add(secondBox.Cells[i]);
            secondBox.Cells[i].myPaths.Add(newPath);
            secondBox.Cells[i].Setup(ClickCell);

            newPath.cells.Add(thirdBox.Cells[i]);
            thirdBox.Cells[i].myPaths.Add(newPath);
            thirdBox.Cells[i].Setup(ClickCell);

            allPaths.Add(newPath);
        }
        
        BuildSquarePaths(firstBox.Cells);
        BuildSquarePaths(secondBox.Cells);
        BuildSquarePaths(thirdBox.Cells);
        
    }

    private void BuildSquarePaths(IReadOnlyList<CrossCell> squareCells)
    {
        for (var i = 0; i < squareCells.Count - 1; i++)
        {
            if (i % 2 != 0 && i != 0) continue;
            var newPath = new CrossPath()
            {
                cells = new List<CrossCell>()
            };
            newPath.cells.Add(squareCells[i]);
            squareCells[i].myPaths.Add(newPath);
            
            newPath.cells.Add(squareCells[i + 1]);
            squareCells[i+ 1].myPaths.Add(newPath);

            var iPlus2Index = i + 2;
            
            if (i + 2 == squareCells.Count)
                iPlus2Index = 0;
            
            newPath.cells.Add(squareCells[iPlus2Index]);
            squareCells[iPlus2Index].myPaths.Add(newPath);
            allPaths.Add(newPath);
        }
    }
    private void ClickCell(CrossCell cell)
    {
        if(cell.cellType != CellType.Empty) return;
        
        switch (boardStateController.gameState)
        {
            case GameState.NotStarted:
                break;
            case GameState.BlueTurn:
                cell.SetState(CellType.Blue);
                break;
            case GameState.RedTurn:
                cell.SetState(CellType.Red);
                break;
            case GameState.Ended:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        CheckCellPaths(cell);
        boardStateController.ChangeTurn();
    }

    private void CheckCellPaths(CrossCell cell)
    {

        foreach (var path in cell.myPaths)
        {
            var itsMatchedPath = true;
            foreach (var pathCell in path.cells.Where(pathCell => pathCell.cellType == CellType.Empty || pathCell.cellType != cell.cellType))
            {
                itsMatchedPath = false;
                continue;
            }
            
            if (!itsMatchedPath) continue;
            
            foreach (var pathCell in path.cells)
            {
                var currentParticle = Instantiate(particle);
                currentParticle.transform.position = pathCell.transform.position;
            }

            Win();
            boardStateController.EndGame();
        }
    }

    private void Win()
    {
        switch (boardStateController.gameState)
        {
            case GameState.NotStarted:
                break;
            case GameState.BlueTurn:
                print("Blue win!");
                OnWin?.Invoke(GameState.BlueTurn);
                break;
            case GameState.RedTurn:
                print("Red win!");
                OnWin?.Invoke(GameState.RedTurn);
                break;
            case GameState.Ended:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
    }
}

[Serializable]
public class CrossPath
{
    public List<CrossCell> cells;
}


[Serializable]
public enum CellType
{
    Empty , Red , Blue
}