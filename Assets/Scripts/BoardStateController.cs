﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class BoardStateController : MonoBehaviour
{
    public float turnTime;
    public GameState gameState;
    public float timeSpend = 0;
    
    public static Action<GameState> TurnChange;
    public static BoardStateController Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void Setup()
    {
        gameState = Random.Range(0f, 1f) > 0.5f ? GameState.RedTurn : GameState.BlueTurn;
        TurnChange?.Invoke(gameState);
    }

    private void Update()
    {
        if(gameState== GameState.Ended || gameState == GameState.NotStarted)
            return;
        if (timeSpend >= turnTime)
        {
            ChangeTurn();
        }

        timeSpend += Time.deltaTime;
    }

    public void ChangeTurn()
    {
        timeSpend = 0;
        switch (gameState)
        {
            case GameState.BlueTurn:
                gameState = GameState.RedTurn;
                break;
            case GameState.RedTurn:
                gameState = GameState.BlueTurn;
                break;
            case GameState.NotStarted:
                break;
            case GameState.Ended:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        TurnChange?.Invoke(gameState);
    }

    public void EndGame()
    {
        gameState = GameState.Ended;
    }

}

public enum GameState
{
    NotStarted , BlueTurn , RedTurn , Ended
}