﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CrossCell : MonoBehaviour , IPointerClickHandler
{
    [SerializeField] private GameObject blueDice, redDice;
    public List<CrossPath> myPaths = new List<CrossPath>();
    public CellType cellType = CellType.Empty;
    
    private Action<CrossCell> _onClick;
    public void Setup(Action<CrossCell> click)
    {
        blueDice.SetActive(false);
        redDice.SetActive(false);
        cellType = CellType.Empty;
        
        _onClick = click;
    }

    public void SetState(CellType type)
    {
        cellType = type;
        switch (type)
        {
            case CellType.Empty:
                return;
            case CellType.Blue:
                blueDice.SetActive(true);
                break;
            case CellType.Red:
                redDice.SetActive(true);
                break;
            default:
                break;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _onClick?.Invoke(this);
    }
}
