﻿using System;
using UnityEngine;

public class MainMenuUiController : MonoBehaviour
{
    [SerializeField] private LeaderBoardController leaderBoardController;
 
    public static Action OnGameStart;

    public void GoToLeaderBoard()
    {
        gameObject.SetActive(false);
        leaderBoardController.gameObject.SetActive(true);
    }
    

    public void StartButtonClick()
    {
        gameObject.SetActive(false);
        OnGameStart?.Invoke();
    }

    public void ExitButtonClick()
    {
        Application.Quit();
    }
}
