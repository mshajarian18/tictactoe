﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderBoardMockData : LeaderBoardNetworkController
{
    [SerializeField] private LeaderBoardMockDataSO mockData;
    
    public override Action<LeaderBoardModel> OnPlayerListReceive { get; set; }

    public override void GetPlayersList()
    {
        var playersData = new LeaderBoardModel {items = mockData.items};
        OnPlayerListReceive?.Invoke(playersData);
    }
}
