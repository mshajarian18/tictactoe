﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class LeaderBoardNetworkController : MonoBehaviour
{
    public abstract Action<LeaderBoardModel> OnPlayerListReceive { get; set; }
    public abstract void GetPlayersList();
}

[Serializable]
public class LeaderBoardModel
{
    public List<LeaderBoardItemModel> items;
}
[Serializable]
public class LeaderBoardItemModel
{
    public string score;
    public string name;
}
