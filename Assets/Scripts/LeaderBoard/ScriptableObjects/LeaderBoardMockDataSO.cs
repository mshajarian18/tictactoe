﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class LeaderBoardMockDataSO : ScriptableObject
{
    public List<LeaderBoardItemModel> items;
}
