﻿using UnityEngine;

public class LeaderBoardController : MonoBehaviour
{
    [SerializeField] private MainMenuUiController mainMenu;
    [SerializeField] private LeaderBoardNetworkController leaderBoardNetwork;
    [SerializeField] private Transform leaderBoardRoot;
    [SerializeField] private LeaderBoardRow leaderBoardRow;
    
    private void Start()
    {
        leaderBoardNetwork.GetPlayersList();
    }
    
    private void OnEnable()
    {
        leaderBoardNetwork.OnPlayerListReceive += OnPlayerListReceive;
    }

    private void OnDisable()
    {
        leaderBoardNetwork.OnPlayerListReceive -= OnPlayerListReceive;
    }

    public void Back()
    {
        gameObject.SetActive(false);
        mainMenu.gameObject.SetActive(true);
    }

    private void Setup(LeaderBoardModel leaderBoardModel)
    {
        var lastItems = leaderBoardRoot.GetComponentsInChildren<LeaderBoardRow>();
        foreach (var row in lastItems)
            Destroy(row.gameObject);
        
        for (var i = 0; i < leaderBoardModel.items.Count; i++)
        {
            var rowItem = Instantiate(leaderBoardRow, leaderBoardRoot);
            rowItem.Setup((i + 1).ToString(), leaderBoardModel.items[i].name, leaderBoardModel.items[i].score);
        }
    }

    private void OnPlayerListReceive(LeaderBoardModel leaderBoardModel)
    {
        Setup(leaderBoardModel);
    }
    
}
