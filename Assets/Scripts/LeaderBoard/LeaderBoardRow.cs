﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LeaderBoardRow : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI rankText, nameText, scoreText;

    public void Setup(string rank , string name , string score)
    {
        rankText.text = rank;
        nameText.text = name;
        scoreText.text = score;
    }
}
