﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePlayUiController : MonoBehaviour
{
    [SerializeField] private Animation redTurnAnim, blueTurnAnim;
    [SerializeField] private TextMeshProUGUI maxTimerBlueText , maxTimerRedText;
    [SerializeField] private TextMeshProUGUI winText;
    [SerializeField] private Image redSlider, blueSlider;
    [SerializeField] private GameObject winPanel;
    
    private int _maxTime = 0;
    private void OnEnable()
    {
        BoardStateController.TurnChange += TurnChange;
        BoardController.OnWin += OnWin;
    }
    
    private void OnDisable()
    {
        BoardStateController.TurnChange -= TurnChange;
        BoardController.OnWin -= OnWin;
    }

    private void Start()
    {
        _maxTime = ((int) BoardStateController.Instance.turnTime);
        maxTimerBlueText.text = _maxTime + " S";
        maxTimerRedText.text = _maxTime + " S";
    }

    private void Update()
    {
        switch (BoardStateController.Instance.gameState)
        {
            case GameState.NotStarted:
                break;
            case GameState.BlueTurn:
                blueSlider.fillAmount = (_maxTime - BoardStateController.Instance.timeSpend) / _maxTime;
                redSlider.fillAmount = 0;
                break;
            case GameState.RedTurn:
                redSlider.fillAmount =  (_maxTime - BoardStateController.Instance.timeSpend) / _maxTime;
                blueSlider.fillAmount = 0;
                break;
            case GameState.Ended:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void TurnChange(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.NotStarted:
                break;
            case GameState.BlueTurn:
                blueTurnAnim.Play();
                break;
            case GameState.RedTurn:
                redTurnAnim.Play();
                break;
            case GameState.Ended:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(gameState), gameState, null);
        }
    }
    private void OnWin(GameState gameState)
    {
        StartCoroutine(WaitForWin());
        switch (gameState)
        {
            case GameState.NotStarted:
                break;
            case GameState.BlueTurn:
                winText.text = "Blue Player Won!";
                break;
            case GameState.RedTurn:
                winText.text = "Red Player Won!";
                break;
            case GameState.Ended:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(gameState), gameState, null);
        }
    }

    private IEnumerator WaitForWin()
    {
        yield return new WaitForSeconds(1f);
        winPanel.SetActive(true);
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
}
